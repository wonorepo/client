const fields = [
	{
		type: 'list',
		name: 'make',
		placeholder: 'Make',
		values: [
			'Audi',
			'BMW',
			'Chery',
			'Chevrolet',
			'Citroen',
			'Daewoo',
			'Dodge',
			'Ford',
			'Honda',
			'Hyundai',
			'Infiniti',
			'Jaguar',
			'Jeep',
			'Kia',
			'Land Rover',
			'Lexus',
			'Lada',
			'Toyota'
		],
		multiple: false,
		required: true
	},
	{
		type: 'input',
		input_type: 'text',
		name: 'engine',
		placeholder: 'Engine',
		required: true
	},
	{
		type: 'input',
		input_type: 'text',
		name: 'trim',
		placeholder: 'Trim',
		required: true
	},
	{
		type: 'list',
		name: 'transmission',
		placeholder: 'Transmission',
		values: [
			'Automatic',
			'Manual'
		],
		multiple: false,
		required: false
	},
	{
		type: 'list',
		name: 'body_type',
		placeholder: 'Body type',
		values: [
			'SUV',
			'Sedan',
			'Hatchback',
			'Station wagon',
			'Convertible',
			'Van',
			'Truck'
		],
		multiple: false,
		required: true
	},
	{
		type: 'list',
		name: 'number_of_seats',
		placeholder: 'Number of seats',
		values: [
			'2',
			'4',
			'5',
			'6',
			'7',
			'8'
		],
		multiple: false,
		required: false
	},
	{
		type: 'input',
		input_type: 'number',
		name: 'since',
		placeholder: 'Year of Model (Make)',
		min: 1970,
		max: 2018,
		required: false
	},
	{
		type: 'list',
		name: 'color',
		placeholder: 'Color',
		values: [
			'Red',
			'Blue',
			'Green',
			'Orange',
			'Yellow',
			'Black',
			'White',
			'Other'
		],
		multiple: false,
		required: false
	}
]

export default fields
