const fields = [
	{
		type: 'list',
		name: 'property_type',
		values: [
			'Detached house',
			'Apartment',
			'Boutique hotel',
			'Bungalow',
			'Villa',
			'Guesthouse',
			'Guest apartment',
			'Resort',
			'Loft',
			'Hotel',
			'Semi-detached house',
			'Farmer\'s house',
			'Hut',
			'Hostel',
			'Chalet'
		],
		placeholder: 'Property type',
		multiple: false,
		required: true
	},
	{
		type: 'number',
		name: 'guests',
		min: 1,
		max: 5,
		placeholder: 'Guest available',
		required: false
	},
	{
		type: 'list',
		name: 'available_for_guests',
		values: [
			'Whole property',
			'Room',
			'Place in the room'
		],
		placeholder: 'Available for guests',
		multiple: false,
		required: true
	},
	{
		type: 'number',
		name: 'bedrooms',
		placeholder: 'Number of bedrooms',
		required: false
	},
	{
		type: 'number',
		name: 'beds',
		placeholder: 'Number of beds',
		required: false
	},
	{
		type: 'number',
		name: 'bathrooms',
		placeholder: 'Number of bathrooms',
		required: false
	},
	{
		type: 'checkbox',
		name: 'private_bathroom',
		placeholder: 'Private bathroom',
		required: false
	},
	{
		type: 'double_list',
		name: 'property_details',
		values: [
			'Kitchen',
			'Soap and shampoo',
			'Central heating',
			'AC',
			'Washing machine',
			'Dish washer',
			'WI-FI',
			'Breakfast',
			'Fireplace',
			'Swimming pool',
			'Balcony',
			'Wardrobe',
			'Iron',
			'Hairdryer',
			'Workplace',
			'TV',
			'Baby bed',
			'Highchair',
			'Elevator',
			'Fire alarm',
			'First aid kit',
			'Towels',
			'Own parking',
			'Gym',
			'Jacuzzi'
		],
		placeholder: 'Property details',
		multiple: true,
		required: true
	},
	{
		type: 'double_list',
		name: 'property_rules',
		placeholder: 'Property rules',
		values: [
			'Smoking allowed',
			'Pets allowed',
			'Kids allowed',
			'Events allowed'
		],
		multiple: true,
		required: true
	}
]

export default fields
