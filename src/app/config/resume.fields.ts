const fields = [
	{
		type: 'list',
		name: 'title',
		placeholder: 'Pick category',
		values: [
			'Design',
			'Art',
			'Coding',
			'Hacking'
		],
		multiple: false,
		required: true
	},
	{
		type: 'groupOfCheckbox',
		name: 'Design',
		placeholder: 'Skills you have (up to 5)',
		values: [
			{
				label : 'Adobe Photoshop',
				name :'adobephotoshop',
			 	value : false
			},
			{
				label : '3D design',
				name :'threeddesign',
			 	value : false
			},
			{
				label : 'Adobe Illustrator',
				name :'adobeillustrator',
			 	value : false
			},
			{
				label : 'Graphic design',
				name :'graphicdesign',
			 	value : false
			},
			{
				label : 'Adobe InDesign',
				name :'adobeindesign',
			 	value : false
			},
			{
				label : 'UI/UX design',
				name :'uiuxdesign',
			 	value : false
			},
			{
				label : 'Corel Draw',
				name :'coreldraw',
			 	value : false
			},
			{
				label : 'Animation',
				name :'animation',
			 	value : false
			}
		],
		multiple: true,
		required: true
	},
	{
		type: 'groupOfCheckbox',
		name: 'Art',
		placeholder: 'Skills you have (up to 5)',
		values: [
			{
				label : 'Paint',
				name :'paint',
			 	value : false
			},
			{
				label : 'Draw',
				name :'draw',
			 	value : false
			},
			{
				label : 'Scketch',
				name :'scketch',
			 	value : false
			}
		],
		multiple: true,
		required: true
	},
	{
		type: 'groupOfCheckbox',
		name: 'Coding',
		placeholder: 'Skills you have (up to 5)',
		values: [
			{
				label : 'Angular',
				name :'angular',
			 	value : false
			},
			{
				label : 'Java',
				name :'java',
			 	value : false
			},
			{
				label : 'Objective-C',
				name :'objectivec',
			 	value : false
			},
			{
				label : 'PHP',
				name :'php',
			 	value : false
			},
			{
				label : 'Basic',
				name :'basic',
			 	value : false
			},
			{
				label : 'Javascript',
				name :'javascript',
			 	value : false
			},
			{
				label : 'C++',
				name :'cplusplus',
			 	value : false
			},
			{
				label : 'C#',
				name :'csharp',
			 	value : false
			}
		],
		multiple: true,
		required: true
	},
	{
		type: 'groupOfCheckbox',
		name: 'Hacking',
		placeholder: 'Skills you have (up to 5)',
		values: [
			{
				label : 'Pentagon',
				name :'pentagon',
			 	value : false
			},
			{
				label : 'CIA',
				name :'cia',
			 	value : false
			},
			{
				label : 'FBI',
				name :'fbi',
			 	value : false
			}
		],
		multiple: true,
		required: true
	}
]

export default fields
