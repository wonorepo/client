import { isDevMode } from '@angular/core'

export function isInDevMode():boolean {
  const prod = 'app.wono.io'
  console.log(prod, location.host)
  return location.host != prod
}
