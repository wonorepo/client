import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { MatSnackBar } from '@angular/material'
import { ContractService, Tx } from '../../services/contract.service'
import { DialogService } from '../../components/dialog/dialog.service'
import { ConfirmTxWindow } from '../../components/confirmTx/confirmTx.component'
import { ErrorWindow } from '../../components/error/error.component'
import { AccountService } from '../../services/account.service'
import { AssetService } from '../../services/asset.service'
import { ApiService } from '../../services/api.service'
import { ConfigService } from '../../services/config.service'

import * as io from 'socket.io-client'

@Component({
  selector: 'asset-page',
  templateUrl: './asset.template.html',
  styleUrls: ['./asset.styles.css']
})
export default class AssetPage implements OnInit, OnDestroy {

  sub : any
  asset : any
  owner : any
  user : any
  image : string
  draft : boolean = false
  loading : boolean = true
  status : string = 'Loading...'
  requested : boolean
  extras : any[] = []

  userPublished : boolean
  authorized : boolean = false

  protected avatar : string = 'assets/img/user.svg'

  public minDate = new Date(Date.now())

  constructor ( private account: AccountService,
                private assetService: AssetService,
                private route: ActivatedRoute,
                private api: ApiService,
                private router: Router,
                private dialog: DialogService,
                private contract: ContractService,
                private config: ConfigService,
                public snackBar: MatSnackBar) {}

  ngOnInit() {
    this.authorized = this.account.authorized
    this.sub = this.route.params.subscribe(async params => {
      let id = params['id']
      if (id.includes('draft')) {
        this.draft = true
        id = id.replace('draft-', '')
      }
      const user = this.user = this.account.cachedUserData
      const asset : any = this.asset = await this.assetService.asset(id, this.draft)
      this.extras = Object.keys(asset.extras)

      if (this.draft) {
        const data = await Promise.all([
          this.api.ipfsLink(asset.imageHash),
          this.account.isPublic(user.address)
        ])
        if (asset.imageHash) this.image = data[0]
        this.owner = this.user.address === asset.owner ? user : {adress: asset.owner.address};
        this.userPublished = data[1]
      } else {
        const promises : any = [
          this.api.ipfsLink(asset.imageHash),
          this.api.request('profiles/' + asset.owner),
        ]
        if (this.authorized) {
          promises.push(this.api.request(`profiles/${user.address}/chats`, { filter: `"asset=${id}"`}))
          promises.push(this.account.isPublic(user.address))
        }
        const data : any = await Promise.all(promises)
        if (asset.imageHash) this.image = data[0]
        this.owner = data[1]

        this.userPublished = data[3]
        if (data[2] && data[2].chats) {
          this.requested = !!data[2].chats.length
        }
        if (this.owner.photo) this.owner.photo = this.api.ipfsUrl + this.owner.photoHash
        if (this.authorized && this.owner.address == user.address) this.requested = true
      }

      this.loading = false
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe()
  }

  async publish () {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true
    this.status = 'Storing Asset data into IPFS...'
    delete this.asset.pending
    const hash : string = await this.api.storeIpfs(JSON.stringify(this.asset), true)
    if (!hash) {
      this.snackBar.open('IPFS is not available! Please try again later.', '', {
        duration: 5000,
      })
      this.loading = false
      return
    }
    this.status = 'Publishing Asset into Blockchain...'
    try {
      const tx : Tx = {
        contract: 'AssetFactory',
        method: 'create',
        from: address,
        gasAmount: gasAmount
      }
      const weiPrice = this.contract.web3.utils.toWei(this.asset.price.toString(), 'ether')
      const result = await this.contract.tx(tx, [hash, weiPrice, 0], privateKey)
      this.status = 'Updating data...'
      this.asset.pending = true
      await this.api.send('put', 'asset-drafts/' + this.asset.id, this.asset, password)
      this.snackBar.open('Your asset is being published at the moment and will be available for search soon', '', {
        duration: 5000,
      })
      this.router.navigate(['/assets'])
    } catch (error) {
      throw error
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
  }

  async createDeal (form) {
    const { message, dates } = form
    const links : any = await this.config.getLinks()
    const user : any = this.account.cachedUserData
    const data = {
      members: [this.owner.address, user.address],
      asset: this.asset.id,
      deal: {
        from: + dates.begin,
        until: + dates.end,
        declined: false
      },
      messages: [{
        owner: user.address,
        text: message,
        readed: false
      }]
    }
    const chat = io(links.chat + '?eth_address=' +  user.address)
    chat.on('chat_created', (response : any) => {
      console.log('created', response)
      this.router.navigate(['chat/' + response.id])
    })
    chat.on('connect', () => {
      console.log('connected')
      chat.emit('new_chat', data, response => {
        console.log(response)
        if (!response.error) return
        this.snackBar.open('Request already exists', '', {
          duration: 5000,
        })
        this.requested = true
      })
    })
  }

}
