import { Component } from '@angular/core'
import { ApiService } from '../../services/api.service'
import { AccountService } from '../../services/account.service'
import AppComponent from '../../app.component';
import {SharedService} from '../../services/shared.service';

const placeholder = 'assets/img/placeholder.svg'

@Component({
  selector: 'chats-page',
  templateUrl: './chats.template.html',
  styleUrls: ['./chats.styles.css']
})
export default class ChatsPage {

  loading : boolean = true
  chats : any[] = []
  user : any
  events : any = {
    deal_accepted: 'Deal signed successfully!',
    deal_created: '',
    deal_declined: 'Deal is declined!',
    deal_cancelled: 'Deal is cancelled!'
  }

  protected avatar : string = 'asset/img/user.svg'

  constructor ( private api: ApiService,
                private account: AccountService,
                private _sharedService: SharedService ) {}

  async ngOnInit () {
    const user = this.user = this.account.cachedUserData
    const chats : any = await this.api.request(`profiles/${user.address}/chats`)
    this.chats = chats.chats.map(chat => {
      if (!chat.assetData.imageHash) chat.img = placeholder
      else chat.img = this.api.ipfsUrl + chat.assetData.imageHash
      return chat
    })

    console.log(chats)

    this.loading = false
  }

  public chatSelected(): void {
    this._sharedService.emit(AppComponent.CloseFilters);
  }
}
