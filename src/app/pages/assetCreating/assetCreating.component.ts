import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { MatSnackBar } from '@angular/material'
import { ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, Validators, FormArray, ValidatorFn } from '@angular/forms'

import { AssetService } from '../../services/asset.service'
import { AccountService } from '../../services/account.service'
import { ApiService } from '../../services/api.service'
import { ContractService, Tx } from '../../services/contract.service'
import { DialogService } from '../../components/dialog/dialog.service'
import { ConfirmWindow } from '../../components/confirm/confirm.component'
import { ConfirmTxWindow } from '../../components/confirmTx/confirmTx.component'
import { ErrorWindow } from '../../components/error/error.component'

import appartmentFields from '../../config/appartment.fields'
import transportFields from '../../config/transport.fields'
import resumeFields from '../../config/resume.fields'

@Component({
  selector: 'asset-creating-page',
  templateUrl: './assetCreating.template.html',
  styleUrls: ['./assetCreating.styles.css']
})
export default class AssetCreatingPage {
  asset : any
  editing : boolean = false
  category : string
  categories : any[] = []
  loading : boolean = true
  status : string = 'Loading...'
  private imageSrc : string
  private imageUrl : string
  protected buf : any
  userPublished : boolean
  date : any
  always : boolean = false
  fields : any
  forms : any
  sub : any
  _draft : boolean

  public minDate = new Date(Date.now())

  id

  constructor ( private router: Router,
                private assetService: AssetService,
                private dialog: DialogService,
                private api: ApiService,
                private account: AccountService,
                private contract: ContractService,
                public snackBar: MatSnackBar,
                private route: ActivatedRoute)
  {}

  async ngOnInit () {
		this.forms = {
      '1': this.toFormGroup(appartmentFields, 1),
      '2': this.toFormGroup(transportFields, 2),
      '4': this.toFormGroup(resumeFields, 4)
    }
    this.fields = {
      '1': appartmentFields,
      '2': transportFields,
      '4': resumeFields
    }
    this.sub = this.route.queryParams.subscribe(async (params : any) => {
      const cats : any = await this.assetService.categories()
      this.categories = Object.values(cats.asset_categories)
      this.userPublished = await this.account.isPublic()
      if (params['edit']) {
        this.id = params['edit']
        this._draft = params['draft'] == 'true'
        const asset : any = this.asset = await this.assetService.asset(params['edit'], this._draft)
				this.category = asset.category
        const fields = this.fields[this.category]
				Object.keys(this.forms[this.category].controls).forEach(key => {
          if(this.forms[this.category].controls[key].controls === undefined){
            if(this.forms[this.category].controls[key] === undefined){
              console.log('this.forms[this.category].controls['+key+'] === undefined')
            } else {
              this.forms[this.category].controls[key].setValue(asset[key])
            }
          } else {
            fields.forEach(field => {
              if (field.name != 'title' &&  field.type == 'groupOfCheckbox' && field.name == key) {
                field.values.forEach((checkbox, index) => {
                  Object.keys(asset.extras).forEach(keyExtras => {
                     if(checkbox.name == keyExtras){
                       this.forms[this.category].controls[key].controls[index].setValue(asset.extras[keyExtras].value)
                     }
                  })
                })
              }
                
            })
            
          }
					
				})
        Object.keys(asset.extras).forEach(key => {
          if(this.forms[this.category].controls[key] === undefined){
            console.log('this.forms[this.category].controls['+key+'] === undefined')
          } else {
            if(asset.extras[key].value === undefined){
              console.log('asset.extras['+key+'].value === undefined')
            } else {
              if(this.forms[this.category].controls[key].controls === undefined){
                this.forms[this.category].controls[key].setValue(asset.extras[key].value)
              }
            }
            
          }
        })

        if (asset.imageHash) this.imageUrl = await this.api.ipfsLink(asset.imageHash)
				if (asset.dates.begin == 0) this.alwaysChaged(true)
        else this.forms[this.category].controls.dates.setValue({
          begin: new Date (asset.dates.begin),
          end: new Date (asset.dates.end)
        })
				this.editing = true
      }
      this.loading = false
    })
  }
  countSkillsForm(max = 5){
    const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);
      return totalSelected <= max ? null : { required: true };
    }
    return validator
  }
  ngOnDestroy() {
    this.sub.unsubscribe()
  }

  async draft (data, password) {
    if (this.imageSrc) {
      this.status = 'Storing Asset Images into IPFS...'
      data.imageHash = await this.uploadImage()
    }
    this.status = 'Creating draft...'
    return await this.assetService.draftAsset(data, password)
  }

  async newDraft (form) {
    const password : any = await this.dialog.open(ConfirmWindow)
    if (!password) return

    this.loading = true
    const formData = form.value
    if (this.category == '2') {
      formData.title = `${formData.since} ${formData.make}`
    }
    const draft = await this.draft(this.formatModel(formData), password)
    this.snackBar.open('Draft successfuly created!', '', {
      duration: 5000,
    })
    this.loading = false
    this.router.navigate(['/asset/draft-' + draft.id])
  }

  async deleteAsset () {
    const password : any = await this.dialog.open(ConfirmWindow)
    if (!password) return

    this.loading = true
    const deaft = await this.assetService.deleteAsset(this.id,this._draft)
    this.snackBar.open('Asset'+(this._draft ? '-Draft ':' ')+ 'successfuly deleted!', '', {
      duration: 5000,
    })
    this.loading = false
    this.router.navigate(['/assets?own=true'])
  }
  async updateDraft (form) {
    this.loading = true
    const data = form.value
    if (this.imageSrc) {
      this.status = 'Storing Asset Images into IPFS...'
      data.imageHash = await this.uploadImage()
    }
    const draft = await this.assetService.updateDraft(this.formatModel(data), this.id)
    this.snackBar.open('Draft successfuly edited!', '', {
      duration: 5000,
    })
    this.loading = false
    this.router.navigate(['/asset/draft-' + draft.id])
  }

  async updateAsset (form) {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true
    const data = Object.assign(this.asset, this.formatModel(form.value))
    if (this.imageSrc) {
      this.status = 'Updating Asset Images into IPFS...'
      data.imageHash = await this.uploadImage()
    }

    this.status = 'Updating Asset data into IPFS...'
    const hash : string = await this.api.storeIpfs(JSON.stringify(data), true)
    if (!hash) {
      this.snackBar.open('IPFS is not available! Please try again later.', '', {
        duration: 5000,
      })
      this.loading = false
      return
    }
    this.status = 'Updating Asset into Blockchain...'
    try {
      const txData : Tx = {
        from: address,
        method: 'update',
        contract: 'Asset',
        address: data.eth_address,
        gasAmount: gasAmount
      }
      const result = await this.contract.tx(txData, [hash], privateKey)
      this.snackBar.open('Your asset is being updated at the moment and will be available for search soon!', '', {
        duration: 5000,
      })
      this.router.navigate(['/assets'])
    } catch (error) {
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
  }

  async publish (form) {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true
    const formData = form.value
    if (this.category == '2') {
      formData.title = `${formData.since} ${formData.make} ${formData.model}`
    }
    const draft = await this.draft(this.formatModel(formData), password)
    if (!draft) {
      this.snackBar.open('Server is not available! Please try again later.', '', {
        duration: 5000,
      })
    }
    this.status = 'Storing Asset data into IPFS...'
    delete draft.pending
    const hash : string = await this.api.storeIpfs(JSON.stringify(draft), true)
    if (!hash) {
      this.snackBar.open('IPFS is not available! Please try again later.', '', {
        duration: 5000,
      })
      this.loading = false
      return
    }
    this.status = 'Publishing Asset into Blockchain...'
    try {
      const weiPrice = this.contract.web3.utils.toWei(draft.price.toString(), 'ether')
      const txData : Tx = {
        contract: 'AssetFactory',
        method: 'create',
        from: address,
        gasAmount: gasAmount
      }
      const result = await this.contract.tx(txData,
        [hash, weiPrice, this.category == '4' ? 3 : 0], // для фриланса вместо 0 - 3
        privateKey);
      this.status = 'Updating data...'
      draft.pending = true
      await this.api.send('put', 'asset-drafts/' + draft.id, draft, password)
      this.snackBar.open('Your asset is being published at the moment and will be available for search soon!', '', {
        duration: 5000,
      })
      this.router.navigate(['/assets'])
    } catch (error) {
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
  }

  readFile (event) {
    if (!event.target.files || !event.target.files[0]) return

    const file = event.target.files[0]

    const urlReader = new FileReader()
    urlReader.onload = e => {
      this.imageSrc = urlReader.result
    }
    urlReader.readAsDataURL(file)

    const bufferReader = new FileReader()
    bufferReader.onloadend = e => {
      this.buf = bufferReader.result
    }
    bufferReader.readAsArrayBuffer(file)
  }

  async uploadImage () {
    const hash : string = await this.api.storeIpfs(this.buf)
    if (!hash) this.snackBar.open('IPFS is not available! Please try again later.', '', {
      duration: 5000,
    })
    return hash
  }

   toFormGroup (fields : any[], category: number) {
    const group : any = {
      desc: new FormControl('', Validators.required),
      price: new FormControl('', Validators.required),
      dates: new FormControl(''),
      location: new FormControl('', Validators.required)
    }
    if (category != 4) group.per = new FormControl('', Validators.required)
    if (category != 2) group.title = new FormControl('', Validators.required)

    fields.forEach(field => {
      if(field.type === 'groupOfCheckbox'){
        const controls = field.values.map(c => new FormControl(false));
        group[field.name] = new FormArray(controls, this.countSkillsForm(5))
      } else {
        group[field.name] = new FormControl(field.value || '', field.required ? Validators.required : null)
      }
    })

    return new FormGroup(group)
  }

  formatModel (data) {
    const model : any = {
      category: +this.category,
      title: data.title,
      desc: data.desc,
      price: data.price,
      per: data.per,
      location: data.location,
      extras: {},
      imageHash: data.imageHash
    }
    if (data.dates && data.dates.begin) {
      model.dates = {
        begin: + data.dates.begin,
        end: + data.dates.end
      }
    } else if(this.category == '4'){
      model.dates = { begin: new Date() }
    } 
    else model.dates = { begin: 0 }

    if (this.category == '2') {
      model.title = `${data.since} ${data.make}`
    }

    const fields = this.fields[this.category]
    if(this.category == '4'){
      fields.forEach(field => {
        if (field.name != 'title' &&  field.type == 'groupOfCheckbox' && field.name == data.title) {
          field.values.forEach((checkbox, index) => {
            return model.extras[checkbox.name] = {
              title: checkbox.label,
              value: data[field.name][index]
            }
          })
        }
          
      })
    } else {
      fields.forEach(field => {
        if (!data[field.name]) return
        model.extras[field.name] = {
          title: field.placeholder,
          value: data[field.name]
        }
      })
    }
    return model
  }

  alwaysChaged (value) {
    this.always = value
    this.forms[this.category].controls.dates.setValue(null)
    if (value) this.forms[this.category].controls.dates.setValue(value ? new Date(0) : null)
  }
}
