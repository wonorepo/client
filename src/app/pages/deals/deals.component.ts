import { Component } from '@angular/core'
import { ApiService } from '../../services/api.service'
import { AccountService } from '../../services/account.service'
import { DialogService } from '../../components/dialog/dialog.service'
import { ConfirmTxWindow } from '../../components/confirmTx/confirmTx.component'
import { ErrorWindow } from '../../components/error/error.component'
import { ContractService, Tx } from '../../services/contract.service'

@Component({
  selector: 'deals-page',
  templateUrl: './deals.template.html',
  styleUrls: ['./deals.styles.css']
})
export default class DealsPage {

  deals : any[]
  loading : boolean = true
  roles = ['Vendor', 'Customer']
  status = 'Loading...'

  constructor ( private api: ApiService,
                private account: AccountService,
                private dialog : DialogService,
                private contract: ContractService) {}

  async ngOnInit () {
    const user = this.account.cachedUserData
    const address = user.address
    const data : any = await this.api.request('deals', { filter: `'partyA="${address}" OR partyB="${address}"'`})
    this.deals = data.deals.map(deal => {
      deal.state = deal.cancelled ? 'Cancelled' : this.state(deal.start, deal.end)
      this.api.ipfsLink(deal.assetData.imageHash).then(link => deal.image = link)
      deal.start = new Date(deal.start).toLocaleDateString('en-US', { month: 'long', day: 'numeric' })
      deal.end = new Date(deal.end).toLocaleDateString('en-US', { month: 'long', day: 'numeric', year: 'numeric' })
      deal.myRole = deal.owner == user.address ? 0 : 1
      deal.companion = deal.members.find(member => member.address != user.address)
      deal.disputed = deal.confirmedByA < 0 || deal.confirmedByB < 0
      if (!deal.disputed) {
        deal.confirmed = deal.myRole == 0 ? !!+deal.confirmedByA : !!+deal.confirmedByB
      }
      return deal
    })
    console.log(data.deals)
    this.loading = false
    console.log(this.deals)
  }

  state (start : number, end : number) {
    const now = Date.now()

    if (now < start) return 'Upcoming'
    if (end < now) return 'Ended'
    return 'Ongoing'
  }

  async cancelDeal (deal) {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true

    try {
      this.status = `Cancelling deal...`
      const txData : Tx = {
        contract: 'SimpleDeal',
        method: 'cancel',
        from: address,
        gasAmount: gasAmount,
        address: deal.eth_address
      }
      const cancel = await this.contract.tx(txData, [], privateKey)
    } catch (error) {
      console.error(error)
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
  }

  async confirmDeal (deal) {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true

    try {
      this.status = `Confirm deal...`
      const txData : Tx = {
        contract: 'SimpleDeal',
        method: 'confirm',
        from: address,
        address: deal.eth_address,
        gasAmount: gasAmount
      }
      const cancel = await this.contract.tx(txData, [], privateKey)
    } catch (error) {
      console.error(error)
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
  }

  async disputDeal (deal) {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true

    try {
      this.status = `Dispute deal...`
      const txData : Tx = {
        contract: 'SimpleDeal',
        method: 'dispute',
        from: address,
        address: deal.eth_address,
        gasAmount: gasAmount
      }
      const cancel = await this.contract.tx(txData, [], privateKey)
    } catch (error) {
      console.error(error)
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
  }

}
