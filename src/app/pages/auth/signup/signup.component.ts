import { Component } from '@angular/core'
import { MatSnackBar } from '@angular/material'
import { AccountService } from '../../../services/account.service'

@Component({
  selector: 'signup-page',
  templateUrl: './signup.template.html',
  styleUrls: ['./signup.styles.css']
})
export default class SignupPage {

  states : any = {
    first: 'first',
    seed: 'seed',
    confirm: 'confirm',
    creating: 'creating',
    end: 'end'
  }
  state : string = this.states.first
  address : string = ''
  password : string = ''

  public seedphrase : string = ''
  public splittedSeed : string[] = []
  public confirm : string[] = []

  constructor ( public account: AccountService,
                public snackBar: MatSnackBar) {
    this.createSeed()
  }

  createSeed () {
    const seed = this.seedphrase = this.account.seedphrase
    this.splittedSeed = seed.split(' ').sort(() => Math.random() - 0.5)
  }

  add (word : string) {
    const index = this.splittedSeed.indexOf(word)
    this.splittedSeed.splice(index, 1)
    this.confirm.push(word)
    if (!this.splittedSeed.length && this.confirm.join(' ') === this.seedphrase) {
      this.createWallet()
    }
  }

  remove (word : string) {
    const index = this.confirm.indexOf(word)
    this.confirm.splice(index, 1)
    this.splittedSeed.push(word)
  }

  async createWallet () {
    this.state = this.states.creating
    const data = await this.account.createWallet(this.password, this.seedphrase)
    this.address = data.address
    this.state = this.states.end
  }

  get confirmed () {
    return this.confirm.join(' ') == this.seedphrase
  }

  copied (event) {
    this.snackBar.open('Copied', 'OK', {
      duration: 3000,
    })
  }
}
