import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { AccountService } from '../../../services/account.service'

@Component({
  selector: 'signin-page',
  templateUrl: './signin.template.html',
  styleUrls: ['./signin.styles.css']
})
export default class SigninPage {

  private show : boolean
  private invalid : boolean = false

  constructor (private router: Router, private account: AccountService) {}

  ngOnInit () {
    this.show = !localStorage.getItem('skip_show')
  }

  async signin (password) {
    this.invalid = false
    try {
      const address = await this.account.signin(password)
      if (!address) throw new Error()

      this.router.navigate(['/'])
    } catch (error) {
      console.error(error)
      this.invalid = true
    }
  }

  disableShow () {
    localStorage.setItem('skip_show', 'true')
    this.show = false
  }
}
