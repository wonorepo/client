import { Component } from '@angular/core'
import { AccountService } from '../../../services/account.service'

@Component({
  selector: 'recovery-page',
  templateUrl: './recovery.template.html',
  styleUrls: ['./recovery.styles.css']
})
export default class RecoveryPage {

  states : any = {
    first: 'first',
    pass: 'pass',
    recovering: 'recovering',
    end: 'end'
  }
  state : string = this.states.first
  address : string
  invalid : boolean = false

  constructor ( public account: AccountService) {}

  async recover (seedphrase: string, password: string) {
    this.state = this.states.recovering
    const data = await this.account.createWallet(password, seedphrase, true)
    this.address = data.address
    this.state = this.states.end
  }

  checkSeed (seed) {
    this.invalid = false
		const valid = this.account.validSeed(seed)
    if (valid) this.state = this.states.pass
    else this.invalid = true
  }
}
