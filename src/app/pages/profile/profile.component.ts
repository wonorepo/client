import { Component } from '@angular/core'
import { AccountService } from '../../services/account.service'
import { DialogService } from '../../components/dialog/dialog.service'
import { MatSnackBar } from '@angular/material'
import { ApiService } from '../../services/api.service'
import { StorageService } from '../../services/storage.service'
import { ConfirmWindow } from '../../components/confirm/confirm.component'
import { ConfirmTxWindow } from '../../components/confirmTx/confirmTx.component'
import { PrivateSettingsWindow } from '../../components/private/private.component'
import { ErrorWindow } from '../../components/error/error.component'

@Component({
  selector: 'profile-page',
  templateUrl: './profile.template.html',
  styleUrls: ['./profile.styles.css']
})
export default class ProfilePage {

  private tab : number = 0
  public user : any = {}
  private loading : boolean = true
  private status : string = 'Loading...'
  private address : string
  protected editing : boolean = false
  private imageSrc : string
  protected buf : any
  public maxDate = new Date((new Date()).getUTCFullYear()-16, 11, 31);
  public minDate = new Date(1900, 0, 1);

  synced : boolean = true
  published : boolean = true

  constructor ( public account: AccountService,
                private dialog : DialogService,
                private api: ApiService,
                private storage: StorageService,
                public snackBar: MatSnackBar) {}

  async ngOnInit () {
    const address = this.address = this.storage.getItem('address')
    this.user = await this.account.userData(address)
    console.log(this.user)
    await this.syncCheck()
    this.loading = false
  }

  async syncCheck () {
    const user = this.user
    const userFileds = Object.keys(user)
    try {
      const publicUser = await this.api.request('profiles/' + this.address)
      this.synced = this.account.synced(user, publicUser)
      console.log(this.synced, publicUser, this.user)
    } catch(error) {
      console.log(error)
      this.published = false
      this.synced = false
      console.log('Wallet is not public!')
    }
  }

  async save () {
    const cached = this.account.cachedUserData
    if (JSON.stringify(this.user) == cached && !this.imageSrc) {
      this.snackBar.open('Nothing to update!', 'OK', {
        duration: 5000,
      })
      this.editing = false
      return
    }

    const password = await this.dialog.open(ConfirmWindow)
    if (!password) return

    this.loading = true

    if (this.imageSrc) this.user.photoHash = await this.uploadImage()
    this.user = await this.account.updateUser(this.user, password)
    this.loading = this.editing = false
    this.snackBar.open('Draft successfuly updated!', 'OK', {
      duration: 5000,
    })
    await this.syncCheck()
  }

  async privateSettings (form) {
    console.log(form)
    const updated = await this.dialog.open(PrivateSettingsWindow, { user: this.user })
    console.log(updated)
  }

  async publish () {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true
    this.status = 'Storing Profile Data into IPFS...'
    delete this.user.pending
    const hash : string = await this.api.storeIpfs(JSON.stringify(this.user), true)
    if (!hash) this.snackBar.open('IPFS is not available! Please try again later.', '', {
      duration: 5000,
    })
    this.status = 'Publishing Profile into Blockchain...'
    try {
      let result

      if (this.published)
        result = await this.account.updateProfile(hash, address, privateKey, gasAmount)
      else
        result = await this.account.publicProfile(hash, address, privateKey, gasAmount)

      this.status = 'Updating data...'
      this.user.pending = true

      await this.account.updateUser(this.user, password)
      this.snackBar.open('Profile syncing in progress.', '', {
        duration: 5000,
      })
    } catch (error) {
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
    await this.syncCheck()
  }

  get birthday () {
    return this.user.birthday.value
      ? new Date(this.user.birthday.value).toLocaleDateString()
      : 'No Data'
  }

  readFile (event) {
    if (!event.target.files || !event.target.files[0]) return

    const file = event.target.files[0]

    const urlReader = new FileReader()
    urlReader.onload = e => {
      this.imageSrc = urlReader.result
    }
    urlReader.readAsDataURL(file)

    const bufferReader = new FileReader()
    bufferReader.onloadend = e => {
      this.buf = bufferReader.result
    }
    bufferReader.readAsArrayBuffer(file)
  }

  async uploadImage () {
    const hash : string = await this.api.storeIpfs(this.buf)
    if (!hash) this.snackBar.open('IPFS is not available! Please try again later.', '', {
      duration: 5000,
    })
    return hash
  }

  copy (el) {
    el.select()
    document.execCommand('copy')
    this.snackBar.open('Copied', 'OK', {
      duration: 3000,
    })
  }

}
