import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetService } from '../../services/asset.service';
import { AccountService } from '../../services/account.service';
import appartmentFields from '../../config/appartment.fields';
import transportFields from '../../config/transport.fields';
import resumeFields from '../../config/resume.fields';
import {isArray} from 'util';
import { SharedService } from '../../services/shared.service'
import AppComponent from '../../app.component';

const trans = item => ({ name: item.name, type: item.type });

@Component({
  selector: 'assets-page',
  templateUrl: './assets.template.html',
  styleUrls: ['./assets.styles.css']
})
export default class AssetsPage {

  assets: any[] = [];
  drafts: any[] = [];
  active: any[] = [];
  search = '';
  sub: any;
  own = false;
  categories: any[] = [];
  title: any = 'Assets';
  filterDates: string;
  filter: any = {};
  categoryNames: string[] = [];
  loading = true;
  extraFields: any = {};
  groupCheckboxArray: any = {};
  user : any;
  authorized : boolean = false

  constructor ( private account: AccountService,
                private assetService: AssetService,
                private route: ActivatedRoute,
                private router: Router,
                private _sharedService: SharedService) {
      const fields = appartmentFields.map(trans).concat(transportFields.map(trans));
      fields.forEach(item => {
        this.extraFields[item.name] = item.type;
      });
      resumeFields.forEach(field => {
        if(field.name != 'title'){
          this.extraFields[field.name] = field.type
          if (field.type == 'groupOfCheckbox') {
            this.groupCheckboxArray[field.name] = field.values
          }
        }
      });
    }

  async ngOnInit () {
    this.authorized = this.account.authorized
    this.sub = this.route.queryParams.subscribe(async (params: any) => {
      this.search = '';
      const own = this.own = params['own'];
      const paramData: any = { limit: 500 };
      this.user = this.account.cachedUserData
      if (own) {
        this._sharedService.emit(AppComponent.CategoryChangeName, 1);
        this.assets = [];
        const data = await Promise.all([
          this.assetService.drafts(paramData),
          this.assetService.myAssets(paramData)
        ]);
        this.drafts = data[0].map(asset => {
          asset.draft = true;
          return asset;
        });
        this.active = data[1];
        this.title = 'My assets';
        this.assets = this.active.concat(this.drafts);
      } else {
        const cats: any = await this.assetService.categories();
        this.categories = Object.values(cats.asset_categories);
        this.categoryNames = this.categories.map(cat => cat.name);

        if (params.search) paramData.search = this.search = params.search
        this.title = 'Search: ' + this.search
        if (params.filter) {
          this.filter = JSON.parse(params.filter);
          paramData.filter = this.filterString(this.filter);

          this.title = this.categoryNames[this.filter.category - 1] + 's'
          if (this.filter.location) this.title += ' in ' + this.filter.location
          if (this.filter.dates) {
            const from = new Date(this.filter.dates.begin).toLocaleDateString('en-US', { month: 'short', day: 'numeric' })
            const to = new Date(this.filter.dates.end).toLocaleDateString('en-US', { month: 'short', day: 'numeric' })
            this.filterDates = `${from} - ${to}`
          }
        }
        this.assets = await this.assetService.assets(paramData);
      }
      this.loading = false;
    });
  }

  ngOnDestroy() {
    this._sharedService.emit(AppComponent.CategoryChangeName, 1);
    this.sub.unsubscribe();
  }

  applyFilter () {
    const queryParams = {
      filter: JSON.stringify(this.filter)
    };
    this.router.navigate(['assets'], { queryParams: queryParams });
  }

  filterString (data) {
    const conditions: string[] = [];
    Object.keys(data).forEach(key => {
      if (!data[key]) { return; }
      switch (key) {
        case 'from' :
          conditions.push(`TONUMBER(price) > ${data[key]}`);
          break;
        case 'to' :
          conditions.push(`TONUMBER(price) < ${data[key]}`);
          break;
        case 'location' :
          conditions.push(`REGEX_CONTAINS(location, "^(?i)${data[key]}.*$")`);
          break;
        case 'dates' :
          const dateConditions = [];
          if (data[key].begin) { dateConditions.push(`dates.\`begin\` < ${+ new Date(data[key].begin)}`); }
          if (data[key].end) { dateConditions.push(`dates.\`end\` > ${+ new Date(data[key].end)}`); }
          conditions.push('(' + dateConditions.join(' AND ') + ' OR dates.`begin` = 0)');
          break;
        case 'price' :
          if (!Array.isArray(data[key])) {
            conditions.push(`${key}=${JSON.stringify(data[key])}`);
          } else {
            conditions.push(`(TONUMBER(price) > ${data[key][0]} and TONUMBER(price) < ${data[key][1]})`);
          }
          break;
        default :
          if (!this.extraFields[key]) {
            conditions.push(`${key}=${JSON.stringify(data[key])}`);
            break;
          }
          switch (this.extraFields[key]) {
            case 'list':
              conditions.push(`extras.${key}  is not null and extras.${key}.\`value\` in ${JSON.stringify(data[key])}`);
              break;
            case 'groupOfCheckbox':
              if (!isArray(data[key])) {
                break;
              }
              if(data.title == key){
                data[key].forEach((value, index) => {
                  if(value){
                    conditions.push(`extras.${this.groupCheckboxArray[key][index].name}.\`value\`=${value}`);
                  }
                });
              }
              break;
            case 'double_list':
              if (!isArray(data[key])) {
                break;
              }
              conditions.push(`extras.${key}  is not null`);
              data[key].forEach((value) => {
                conditions.push(`ARRAY_CONTAINS(extras.${key}.\`value\`, "${value}")`);
              });
              break;
            default:
              conditions.push(`extras.${key}  is not null and extras.${key}.\`value\`=${JSON.stringify(data[key])}`);
          }
      }
    });
    const result = '"' + conditions.join(' AND ') + '"';
    return result;
  }

}
