import { Component } from '@angular/core'

@Component({
  selector: 'balance-page',
  templateUrl: './balance.template.html',
  styleUrls: ['./balance.styles.css']
})
export default class BalancePage {}
