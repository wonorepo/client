import { Component } from '@angular/core'
import { AssetService } from '../../services/asset.service'
import { Router } from '@angular/router'
import { ApiService } from '../../services/api.service'
import { SharedService } from '../../services/shared.service'

import resumeFields from '../../config/resume.fields'
import AppComponent from '../../app.component';

@Component({
  selector: 'main-page',
  templateUrl: './main.template.html',
  styleUrls: ['./main.styles.css']
})
export default class MainPage {

  assets : any[] = []
  categories : any[] = []
  filter : any = {}
  loading : boolean = true
  owner : any
  fields : any

  public minDate = new Date(Date.now())

  constructor ( private assetService: AssetService,
                private router: Router,
                private api: ApiService,
                private _sharedService: SharedService) {}

  async ngOnInit () {
    this.fields = {
      '4': resumeFields
    }
    const cats : any = await this.assetService.categories()

    this.categories = Object.values(cats.asset_categories) 

    const data = await Promise.all(this.categories.map(cat => {
      return this.assetService.assets({ filter: `"category=${cat.id}"`, limit: 3 })
    }))

    this.categories.forEach((cat, i) => {
      if (!data[i] || !data[i].length) return
      this.assets.push({
        id: cat.id,
        name: cat.name,
        data: data[i]
      })
    })


    this.loading = false
  }

  search (form, category) {
    const filter = form.value || {}
    filter.category = category
    this._sharedService.emit(AppComponent.CategoryChangeName, category);
    const queryParams = {
      filter: JSON.stringify(filter)
    }
    this.router.navigate(['assets'], { queryParams: queryParams })
  }

}
