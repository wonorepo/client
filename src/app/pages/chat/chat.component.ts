import { Component, OnDestroy } from '@angular/core'
import { MatSnackBar } from '@angular/material'
import { AccountService } from '../../services/account.service'
import { ConfigService } from '../../services/config.service'
import { ApiService } from '../../services/api.service'
import { ActivatedRoute } from '@angular/router'
import { DialogService } from '../../components/dialog/dialog.service'
import { ConfirmTxWindow } from '../../components/confirmTx/confirmTx.component'
import { ErrorWindow } from '../../components/error/error.component'
import { ContractService, Tx } from '../../services/contract.service'

import * as io from 'socket.io-client'
import {SharedService} from '../../services/shared.service';
import AppComponent from '../../app.component';

const count = 10
const fileLimit = 5000000

@Component({
  selector: 'chat-page',
  templateUrl: './chat.template.html',
  styleUrls: ['./chat.styles.css']
})
export default class ChatPage implements OnDestroy {

  sub : any
  asset : any
  assetOwner : any
  messages : any = []
  total: number
  socket : any
  user : any
  chat : any
  deal : any
  companion : any
  protected loading : boolean = true
  protected status : string = 'Loading...'
  protected offset : number = 0

  declined : boolean = false
  cancelled : boolean = false

  tokenBalance : number

  from : string
  until : string

  constructor ( private route: ActivatedRoute,
                private configService: ConfigService,
                private account: AccountService,
                private api: ApiService,
                private dialog : DialogService,
                private contract: ContractService,
                private _sharedService: SharedService,
                public snackBar: MatSnackBar) {}

  ngAfterViewInit () {
    this.sub = this.route.params.subscribe(async params => {
      const user = this.user = this.account.cachedUserData
      const chat : any = this.chat = await this.api.request(`chats/${params['id']}`)
      this.declined = chat.deal.declined
      const data : any = await Promise.all([
        this.api.request(`chats/${params['id']}/messages`),
        this.api.request(`assets/${chat.asset}`),
        this.api.request(`assets/${chat.asset}/deals`, { filter: `'partyB="${chat.owner}"'`}),
        this.api.request('profiles/' + chat.members.filter(member => member != user.address)),
        this.contract.getValue('Token', 'balanceOf', [user.address]),
        this.configService.getLinks()
      ])
      this.messages = data[0].messages.reverse()
      this.total = data[0].total
      this.asset = data[1]
      this.companion = data[3]
      this.tokenBalance = data[4]
      if (data[2] && data[2].deals && data[2].deals.length) {
        this.deal = data[2].deals[0]
      }
      if (this.deal) this.cancelled = this.deal.cancelled
      console.log(data)
      this.assetOwner = chat.assetOwner
      this.assetOwner.photo = this.api.ipfsUrl + this.assetOwner.photoHash
      const locale = { year: 'numeric', month: 'long', day: 'numeric' }
      this.from = new Date(chat.deal.from).toLocaleDateString('en-US', locale)
      this.until = new Date(chat.deal.until).toLocaleDateString('en-US', locale)

      const socket = this.socket = io(data[5].chat + '?eth_address=' +  this.user.address)
      socket.on('connect', () => {
        socket.emit('join_chat', { chatId: chat.id })
        this.read(this.messages)
        this.scrollDown()
      })
      socket.on('new_msg', async data => {
        if (data.event == 'deal_accepted') {
          await this.updateDeal()
          if (!this.deal) this.deal = {}
        }
        if (data.owner != user.address) this.read([data])
        this.messages.push(data)
        this.scrollDown()
      })
      socket.on('msg_readed', data => {
        this.messages.forEach(msg => {
          if(msg.id == data.id) msg.readed = true
        })
      })
      socket.on('error', error => {
        console.error(error)
      })

      this.loading = false
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe()
    this.socket.emit('leave_chat', { chatId: this.chat.id })
    this._sharedService.emit(AppComponent.ChatsUpdated);
  }

  async loadMessages () {
    this.offset += count
    const data : any = await this.api.request(`chats/${this.chat.id}/messages`, {
      offset: this.offset,
      limit: count
    })
    this.messages.unshift(...data.messages.reverse())
    this.read(data.messages)
  }

  scrollDown () {
    const chatEl = document.getElementById('chat-container')
    setTimeout(() => chatEl.scrollTop = chatEl.scrollHeight)
  }

  send (input, media?) {
    if (!input.value) return
    this.socket.emit('send_msg', {
      chatId: this.chat.id,
      msg: {
        text: input.value,
        media: media,
        readed: false
      }
    }, data => {
      if (!data || data['type'] != 'msg') return
      input.value = ''
    })
  }

  read (messages) {
    messages.forEach(msg => {
      if(msg.owner == this.user.owner || msg.readed) return
      this.socket.emit('read_msg', {id: msg.id}, data => {
        console.log(data)
      })
    })
  }

  async uploadFile (files) {
    if (!files || !files[0]) return
    const file = files[0]

    if (file.type.split('/')[0] != 'image') {
      this.dialog.open(ErrorWindow, { message: 'Sorry, you can attach images in JPG, GIF or PNG only.' })
      return
    }

    if (file.size > fileLimit) {
      this.dialog.open(ErrorWindow, { message: 'File is too big' })
      return
    }

    const formData = new FormData()
    const salt = Math.random().toString(36).substring(7)
    formData.append('image-' + salt, file)

    try {
      const data : any = await this.api.post('media', formData, {
        'Content-Type': 'multipart/form-data; boundary=' + salt
      })

      this.socket.emit('send_msg', {
        chatId: this.chat.id,
        msg: {
          text: files[0].name,
          media: { hash: data.files[0].filename },
          readed: false
        }
      })
    } catch (error) {
      console.error(error)
      this.snackBar.open('Try again later!', 'OK', {
        duration: 5000,
      })
    }
  }

  async createDeal () {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true

    this.status = 'Creating deal...'

    const contrPart = this.chat.members.find(member => member != this.user.address)
    const dealInfo = {
      assetId: this.asset.id,
      start: this.chat.deal.from,
      end: this.chat.deal.until,
      unit: 0,
      partyA: this.user.address,
      partyB: contrPart,
      chatId: this.chat.id
    }

    const hash : string = await this.api.storeIpfs(dealInfo, true)
    if (!hash) this.snackBar.open('IPFS is not available! Please try again later.', '', {
      duration: 5000,
    })
    try {
      const txData : Tx = {
        method: 'create',
        contract: 'DealFactory',
        from: address,
        gasAmount: 20
      }
      const result = await this.contract.tx(txData,
        [ this.asset.category == '4' ? 1 : 0, this.asset.eth_address, contrPart, hash, dealInfo.start / 1000, dealInfo.end / 1000],
        privateKey); // для фриланса 1, в нормальной реализации сделать так чтобы сам asset хранил в себе эту информацию.
      await this.updateDeal ()
      this.socket.emit('send_msg', {
        chatId: this.chat.id,
        msg: {
          text: '',
          readed: false,
          event: 'deal_accepted'
        }
      }, data => {
        if (!data || data['type'] != 'msg') return
      })
    } catch (error) {
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
  }

  async acceptDeal () {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true

    try {
      this.status = 'Publishing deal to blockchain…'
      const days = Math.ceil((this.deal.end - this.deal.start) / 86400000)
      const transferTxData : Tx = {
        contract: 'Token',
        method: 'transfer',
        from: address,
        gasAmount: gasAmount
      }
      const weiAmount = this.contract.web3.utils.toWei(this.asset.price * days * 1.1 + '', 'ether')
      const transferResult = await this.contract.tx(transferTxData, [this.deal.eth_address, weiAmount], privateKey)
      const signTxData : Tx = {
        contract: 'SimpleDeal',
        method: 'sign',
        from: address,
        address: this.deal.eth_address,
        gasAmount: gasAmount
      }
      const signResult = await this.contract.tx(signTxData, [], privateKey)
      await this.updateDeal()
      this.deal.signedByB = true
      this.socket.emit('send_msg', {
        chatId: this.chat.id,
        msg: {
          text: '',
          readed: false,
          event: 'deal_created'
        }
      }, data => {
        if (!data || data['type'] != 'msg') return
      })
    } catch (error) {
      console.error(error)
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
  }

  async updateDeal () {
    const data : any = this.api.request(`assets/${this.chat.asset}/deals`, { filter: `'partyB="${this.chat.owner}"'`})
    if (data.deals && data.deals.length) {
      this.deal = data.deals[0]
    }
  }

  async decline () {
    this.socket.emit('decline_deal', { chatId: this.chat.id }, data => {
      console.log(data)
      this.declined = true
      this.socket.emit('send_msg', {
        chatId: this.chat.id,
        msg: {
          text: '',
          readed: false,
          event: 'deal_declined'
        }
      })
    })
  }

  async cancelDeal () {
    const { address, privateKey, gasAmount, password } : any = await this.dialog.open(ConfirmTxWindow)
    if (!privateKey) return

    this.loading = true

    try {
      this.status = `Cancelling deal...`
      const txData : Tx = {
        contract: 'SimpleDeal',
        method: 'cancel',
        address: this.deal.eth_address,
        from: address,
        gasAmount: gasAmount,
      }
      const cancel = await this.contract.tx(txData, [], privateKey)
      await this.updateDeal()
      this.deal.cancelled = true
      this.socket.emit('send_msg', {
        chatId: this.chat.id,
        msg: {
          text: '',
          readed: false,
          event: 'deal_cancelled'
        }
      }, data => {
        if (!data || data['type'] != 'msg') return
      })
    } catch (error) {
      console.error(error)
      this.dialog.open(ErrorWindow, { message: error.message })
    }
    this.loading = false
    this.status = 'Loading...'
  }

}
