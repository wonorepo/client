import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { RouterModule, Routes } from '@angular/router'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { BrowserModule } from '@angular/platform-browser'

// Service
import { AccountService } from './services/account.service'
import { StorageService } from './services/storage.service'
import { ApiService } from './services/api.service'
import { AssetService } from './services/asset.service'
import { ContractService } from './services/contract.service'
import { ConfigService } from './services/config.service'
import { LoginGuard } from './services/guard.service'
import { DialogService } from './components/dialog/dialog.service'
import { Store } from './services/store.service'
import { SharedService } from './services/shared.service'

// Angular Material
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatIconModule } from '@angular/material/icon'
import { MatTabsModule } from '@angular/material/tabs'
import { MatCardModule } from '@angular/material/card'
import { MatInputModule } from '@angular/material/input'
import { MatSelectModule } from '@angular/material/select'
import { MatSliderModule } from '@angular/material/slider'
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatNativeDateModule } from '@angular/material'
import { MatChipsModule } from '@angular/material/chips'
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { MatExpansionModule } from '@angular/material/expansion'
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker'
import { MatMenuModule } from '@angular/material/menu'
import { ClipboardModule } from 'ngx-clipboard'

// Pages
import AppComponent from './app.component'
import MainPage from './pages/main/main.component'
import ProfilePage from './pages/profile/profile.component'
import SigninPage from './pages/auth/signin/signin.component'
import SignupPage from './pages/auth/signup/signup.component'
import RecoveryPage from './pages/auth/recovery/recovery.component'
import AssetsPage from './pages/assets/assets.component'
import AssetPage from './pages/asset/asset.component'
import AssetCreatingPage from './pages/assetCreating/assetCreating.component'
import ChatsPage from './pages/chats/chats.component'
import ChatPage from './pages/chat/chat.component'
import DealsPage from './pages/deals/deals.component'

// Components
import PageHeaderComponent from './components/header/header.component';
import WonoLogo from './components/logo.component'
import WonoCounter from './components/counter/counter.component'
import AssetItem from './components/asset/asset.component'
import Dialog from './components/dialog/dialog.component'
import { ConfirmWindow } from './components/confirm/confirm.component'
import { ConfirmTxWindow } from './components/confirmTx/confirmTx.component'
import { PrivateSettingsWindow } from './components/private/private.component'
import { ErrorWindow } from './components/error/error.component'
import { FilterWindowComponent } from './components/filter/filter.component';

// Other External Components
import { NouisliderModule } from 'ng2-nouislider'

const appRoutes : Routes = [
  { path: '', component: MainPage },
  { path: 'auth/signin', component: SigninPage },
  { path: 'auth/signup', component: SignupPage },
  { path: 'auth/recovery', component: RecoveryPage },
  { path: 'assets', component: AssetsPage },
  { path: 'asset/:id', component: AssetPage },
  {
    path: 'deals',
    component: DealsPage
  },
  {
    path: 'profile',
    component: ProfilePage,
    canActivate: [LoginGuard]
  },
  {
    path: 'create/asset',
    component: AssetCreatingPage,
    canActivate: [LoginGuard]
  },
  {
    path: 'chats',
    component: ChatsPage,
    canActivate: [LoginGuard]
  },
  {
    path: 'chat/:id',
    component: ChatPage,
    canActivate: [LoginGuard]
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  },
]

@NgModule({
  declarations: [
    // Pages
    AppComponent,
    MainPage,
    SigninPage,
    SignupPage,
    RecoveryPage,
    ProfilePage,
    AssetsPage,
    AssetPage,
    AssetCreatingPage,
    ChatsPage,
    ChatPage,
    DealsPage,

    // Components
    PageHeaderComponent,
    WonoCounter,
    WonoLogo,
    AssetItem,
    Dialog,
    ConfirmWindow,
    ConfirmTxWindow,
    PrivateSettingsWindow,
    ErrorWindow,
    FilterWindowComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,

    // Angular Material
    MatSidenavModule,
    MatIconModule,
    MatTabsModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatExpansionModule,
    SatDatepickerModule,
    SatNativeDateModule,
    MatSliderModule,
    MatMenuModule,
    NouisliderModule,
    ClipboardModule
  ],
  providers: [
    LoginGuard,
    AccountService,
    AssetService,
    StorageService,
    ApiService,
    ConfigService,
    DialogService,
    ContractService,
    Store,
    SharedService
  ],
  entryComponents: [
    ConfirmWindow,
    ConfirmTxWindow,
    PrivateSettingsWindow,
    ErrorWindow
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
