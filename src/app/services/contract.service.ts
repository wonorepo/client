import { Injectable } from '@angular/core'
import { ConfigService } from './config.service'
import * as utils from 'ethereumjs-util'
import Web3 from 'web3'

export declare type Tx = {
  contract: string,
  address?: string,
  method: string,
  from: string,
  gasAmount?: string|number,
  gasPrice?:string|number
}

@Injectable()
export class ContractService {

  public web3 : any
  private contracts : any = {}

  constructor (private config: ConfigService) {}

  async init () {
    const data : any = await this.config.getLinks()
    this.web3 = new Web3(data.blockchain)
  }

  async getWeb3 () {
    if (!this.web3) await this.init()
    return this.web3
  }

  async getValue (contractName: string, key : string, args? : any[]) {
    if (!this.web3) await this.init()
    let contract = this.contracts[contractName]

    if (!contract) {
      const data = await Promise.all([
        this.config.getAbi(contractName),
        this.config.getAdresses()
      ])
      contract = this.contracts[contractName] = new this.web3.eth.Contract(data[0], data[1][contractName].addr)
    }
    args = args || []
    const value = await contract.methods[key](...args).call()
    return this.web3.utils.fromWei(value, 'ether')
  }

  async balance (address : string) {
    if (!this.web3) await this.init()
    const balance = await this.web3.eth.getBalance(address)
    return this.web3.utils.fromWei(balance, 'ether')
  }

  async tx (txData: Tx, args: any[], privateKey: string) {
    if (!this.web3) await this.init()
    const promises = [this.config.getAbi(txData.contract)]
    if (!txData.address) promises.push(this.config.getAdresses())

    const data = await Promise.all(promises)
    const contractAddr = txData.address || data[1][txData.contract].addr
    const contract = new this.web3.eth.Contract(data[0], contractAddr)
    const receipt = contract.methods[txData.method](...args).encodeABI()
    const gasPrice = this.web3.utils.toWei(txData.gasAmount + '', 'gwei')
    const tx = {
      gasPrice: gasPrice,
      gas: "3000000",
      data: receipt,
      from: txData.from,
      to: contractAddr
    }
    console.log(tx)
    const signedTx = await this.web3.eth.accounts.signTransaction(tx, privateKey)
    const sendedTx = await this.web3.eth.sendSignedTransaction(signedTx.rawTransaction)
    return sendedTx
  }

}
