import { Router, CanActivate } from '@angular/router'
import { Injectable } from '@angular/core'
import { AccountService } from './account.service'

@Injectable()
export class LoginGuard implements CanActivate {

  constructor (private router: Router, private account: AccountService) {}

  canActivate () {
    const auth = this.account.authorized
    if (!auth) this.router.navigate(['/auth/signin'])
    return auth
  }
}
