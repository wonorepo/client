import { Injectable } from '@angular/core'
import { Subject, Observable } from 'rxjs'
import { map, distinctUntilChanged, shareReplay, scan } from 'rxjs/operators'
import { get, omit, pipe, isEqual } from 'lodash'

declare type Filters = {}
declare type Action = {}

export const reducer = () =>
  scan<any>((state, action) => {
    let next
    switch (action.type) {
      case 'SET':
        next = action.payload
        break
      case 'UPDATE':
        next = { ...state, ...action.payload }
        break
      case 'DELETE':
        next = omit(state, action.payload)
        break
      default:
        next = state
        break
    }

    return next
}, {})

export const slice = path => pipe (
  map(state => get(state, path, null)),
  distinctUntilChanged(isEqual)
)

@Injectable({
  providedIn: 'root'
})
export class Store {
  state: Observable<any>
  actions: Subject<Action> = new Subject()

  constructor () {
    this.state = this.actions.pipe(
      reducer(),
      shareReplay(1)
    )
  }

  public dispatch (action: Action) {
    this.actions.next(action)
  }

  public select (path: string) {
    return this.state.pipe(slice(path))
  }
}

export type State = {
  data: { id: number },
  list: number[],
  filters: Filters,
  watched: { [id: number]: boolean }
}

// init state
export const initState: State = {
  data: { id: 1 },
  list: [],
  filters: { seacrh: '', title: null, startDate: 0 },
  watched: {}
}
