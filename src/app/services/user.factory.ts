export type Field = {
	value : string
	private : boolean
}

const emptyField : Field = {
	value: '',
	private: false
}
export class UserData {

	address : string
	firstname : Field = emptyField
	lastname : Field = emptyField
	birthday : Field = emptyField

	constructor (address : string) {
		this.address = address
	}
	
}
