import { Injectable } from '@angular/core'
import { ApiService } from './api.service'
import { StorageService } from './storage.service'

@Injectable()
export class AssetService {

  constructor (private api: ApiService, private storage: StorageService) {}

  async draftAsset (data, password) {
    const wallet = this.storage.walletData(password)
    data.owner = wallet.address

    return await this.api.send('post', 'asset-drafts/', data, password)
  }

  async updateDraft (data, id) {
    return await this.api.send('put', 'asset-drafts/' + id, data)
  }

  async drafts (query) : Promise<any> {
    const address = this.storage.getItem('address')
    const drafts : any = await this.api.request(`profile-drafts/${address}/assets`, query)
    return await this.loadImages(drafts.assets)
  }

  async myAssets (query?) : Promise<any> {
    const address = this.storage.getItem('address')
    const data : any = await this.api.request(`profiles/${address}/assets`, query)
    return await this.loadImages(data.assets)
  }

  async assets (query?) : Promise<any> {
    const data : any = await this.api.request('assets', query)
    return await this.loadImages(data.assets)
  }

  async asset (id : string, draft? : boolean) {
    const path = (draft ? 'asset-drafts/' : 'assets/') + id
    return await this.api.request(path)
  }

  async deleteAsset (id : string, draft? : boolean) {
    const path = (draft ? 'asset-drafts/' : 'assets/') + id
    return await this.api.delete(path)
  }

  async loadImages (assets) {
    const promises = assets.map(async asset => {
      if (asset.imageHash) asset.image = await this.api.ipfsLink(asset.imageHash)
      return asset
    })
    return await Promise.all(promises)
  }

  async categories () {
    return await this.api.request('enums')
  }

}
