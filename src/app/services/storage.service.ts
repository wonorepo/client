// @ts-ignore
import * as crypto from 'crypto'
import bs58 from 'bs58'
import * as utils from 'ethereumjs-util'

import { Injectable, isDevMode } from '@angular/core'

const DEV = isDevMode()

@Injectable()
export class StorageService {

  algorithm : string = 'aes-256-ctr'

  key (key) { return (DEV ? 'dev:' : '') + key}

  setItem (key, value) {
    const _key = this.key(key)
    return localStorage.setItem(_key, value)
  }

  getItem (key) {
    return localStorage.getItem(this.key(key)) || ''
  }

  privateKey (password : string) {
    const encrypted = localStorage.getItem(this.key('key'))
    return this.decrypt(encrypted, password)
  }

  walletData (password : string) {
    const privateKey = this.privateKey(password)
    const privateBuffer = utils.toBuffer(privateKey)
    if (!utils.isValidPrivate(privateBuffer)) throw new Error ('Password is incorrect!')
    const address = utils.bufferToHex(utils.privateToAddress(privateBuffer))
    return { privateKey, address }
  }

  encrypt (data : string, password : string) {
    const cipher = crypto.createCipher(this.algorithm, password)
    let crypted = cipher.update(data, 'utf8', 'hex')
    return crypted += cipher.final('hex')
  }

  decrypt (encrypted : string, password : string) {
    const decipher = crypto.createDecipher(this.algorithm, password)
    let decrypted = decipher.update(encrypted, 'hex', 'utf8')
    decrypted += decipher.final('utf8')

    return decrypted
  }

  storeValue (key : string, value : any) {
    if (typeof value !== 'string') value = JSON.stringify(value)
    localStorage.setItem(this.key(key), value)
  }

  verifyPassword (password : string) {
    const privateKey = this.privateKey(password)
    const privateBuffer = utils.toBuffer(privateKey)
    return utils.isValidPrivate(privateBuffer)
  }

  hexToBase58 (hex : string) {
    const bin = utils.toBuffer(hex.replace('0x', '1220'))
    return bs58.encode(bin)
  }

  base58ToHex (base58 : string) {
    const bin = bs58.decode(base58)
    const hex = bin.toString('hex')
    return hex.replace('1220', '0x')
  }

  signin (password) {
    const { privateKey, address } = this.walletData(password)
    console.log(privateKey)
    const hash = utils.sha256('{}')
    const obj = utils.ecsign(hash, utils.toBuffer(privateKey))
    const signature = utils.toRpcSig(obj.v, obj.r, obj.s)
    localStorage.setItem(this.key('address'), address)
    localStorage.setItem(this.key('signature'), signature)

    return address
  }

  removeItem (key) {
    return localStorage.removeItem(this.key(key))
  }

}
