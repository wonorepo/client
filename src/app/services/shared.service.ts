import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class SharedService {
  private _subjects = new Map<string, Subject<any>>();

  public register(eventName: string): Subject<any> {
    if (!this._subjects.has(eventName)) {
      this._subjects.set(eventName, new Subject<any>());
    }
    return this._subjects.get(eventName);
  }

  public emit(eventName: string, event?: any): void {
    if (this._subjects.has(eventName)) {
      this._subjects.get(eventName).next(event);
    }
  }
}
