import { Injectable } from '@angular/core'
import { StorageService } from './storage.service'
import { Store } from './store.service'
import { ApiService } from './api.service'
import { ContractService, Tx } from './contract.service'

import { UserData } from './user.factory'

import * as lightwallet from 'eth-lightwallet'

const fields = [
  'firstname',
  'lastname',
  'birthday',
  'photoHash'
]

@Injectable()
export class AccountService {

  constructor (
                private storage: StorageService,
                private api: ApiService,
                private contract: ContractService) {}

  get seedphrase () {
    return lightwallet.keystore.generateRandomSeed()
  }

  public async createWallet (password : string, seedphrase : string, recovery? : boolean) : Promise<any> {
    return new Promise((resolve, reject) =>
      lightwallet.keystore.createVault({
        password: password,
        seedPhrase: seedphrase.toLowerCase()!.trim(),
        hdPathString: `m/44'/60'/0'/0`
      }, (err, ks) => {
        if (err) reject(err)
        ks.keyFromPassword(password, async (err, pwDerivedKey) => {
          if (err) reject(err)
          ks.generateNewAddress(pwDerivedKey)

          const address : string = ks.getAddresses()[0]
          const privateKey = '0x' + ks.exportPrivateKey(address, pwDerivedKey)
          const encrypted = this.storage.encrypt(privateKey, password)
          this.storage.setItem('key', encrypted)

          if (recovery) try {
              const data = await this.signin(password)
              resolve(data)
              return
          } catch (error) {
            console.error(error)
          }
          this.storage.signin(password)
          this.api.send('post', 'profile-drafts/', new UserData(address)).then(data => {
            this.storage.setItem('userData', JSON.stringify(data))
            resolve(data)
          })
        })
      }))
  }

  get cachedUserData () {
    const data = this.storage.getItem('userData')
    return data ? JSON.parse(data) : data
  }

  async userData (address : string) {
    const options = {
      headers: {
        'X-Eth-Address': address
      }
    }
    const data : any = await this.api.request(`profile-drafts/${address}`, null, options)
    if (!data) throw new Error ('User is not auth!')

    if (data.photoHash) {
      data.photo = await this.api.ipfsLink(data.photoHash)
    }

    this.storage.setItem('userData', JSON.stringify(data))
    return data
  }

  async updateUser (data, password) {
    const result = await this.api.send('put', `profile-drafts/${data.address}`, data, password)

    if (result.photoHash) {
      result.photo = await this.api.ipfsLink(result.photoHash)
    }

    this.storage.setItem('userData', JSON.stringify(result))
    return result
  }

  async publicProfile (hash, address, privateKey, gasAmount) {
    const txData : Tx = {
      contract: 'UserStorage',
      method: 'addUser',
      from: address,
      gasAmount: gasAmount
    }
    return await this.contract.tx(txData, [address, hash], privateKey)
  }

  async updateProfile (hash, address, privateKey, gasAmount) {
    const txData : Tx = {
      contract: 'UserStorage',
      method: 'updateUser',
      from: address,
      gasAmount: gasAmount
    }
    return await this.contract.tx(txData, [hash], privateKey)
  }

  invalid (user) {
    return fields.find(field => {
      if (typeof user[field] == 'string' && user[field].trim()) return false
      if (typeof user[field].value == 'string' && user[field].value.trim()) return false
      return true
    })
  }

  synced (user, publicUser) {
    return !fields.find(field => {
      if (JSON.stringify(user[field]) != JSON.stringify(publicUser[field])) return true
    })
  }

  async isPublic (address?) {
    if (!address) {
      address = this.cachedUserData.address
    }
    try {
      const user = await this.api.request('profiles/' + address)
      return !!user
    } catch (error) {
      return false
    }
  }

  validSeed (seed) {
    return lightwallet.keystore.isSeedValid(seed)
  }

  async signin (password) {
    const address = this.storage.signin(password)
    return await this.userData(address)
  }

  logout () {
    this.storage.removeItem('signature')
    this.storage.removeItem('address')
    this.storage.removeItem('userData')
  }

  get authorized () {
    return !!this.storage.getItem('signature')
  }

}
