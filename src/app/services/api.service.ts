import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { HttpHeaders } from '@angular/common/http'
import { ConfigService } from './config.service'
import { StorageService } from './storage.service'

import ipfsApi from 'ipfs-api'
import * as utils from 'ethereumjs-util'

declare const Buffer

@Injectable()
export class ApiService {

	_url : string
	_ipfs : any

  _ipfs_domain_specs = {
    protocol: 'https',
    host: 'app.wono.io',
    port: '443',
  	'api-path': '/api/v0/'
	}

	ipfsUrl : string

  constructor (	private http: HttpClient,
								private config: ConfigService,
								private storage: StorageService) {

		this.ipfsUrl = this._ipfs_domain_specs.protocol + '://' + this._ipfs_domain_specs.host + this._ipfs_domain_specs['api-path'] + 'cat/'

	}

	async url (route : string, params?: any) {
		let url = this._url

		if (!url) {
			const links = await this.config.getLinks()
			url = links.api
		}
		this._url = url
		url += `/api/${route}`

		if (params) {
			Object.keys(params).forEach((param, i) => {
				url += i > 0 ? '&' : '?'
				url += param + '=' + params[param]
			})
		}

		return url
	}

	async ipfs () {
		let ipfs = this._ipfs

		if (ipfs) return ipfs
		this._ipfs = ipfsApi(this._ipfs_domain_specs)

		return this._ipfs
	}

  sessionHeaders () : HttpHeaders {
    return new HttpHeaders({
      'X-Signature': this.storage.getItem('signature'),
      'X-Eth-Address': this.storage.getItem('address'),
    })
  }

  async request (route : string, params?: any, httpOptions? : any) {
		let url : string = await this.url(route, params)
		console.log(url)
    const data = await this.http.get(url, httpOptions).toPromise()
    return data
  }
  	async delete (route : string) {
		const url : string = await this.url(route)
		const httpOptions = {
			headers: this.sessionHeaders()
		}
		const response = await this.http.delete(url, httpOptions).toPromise()
    return response
	}

	async send (method: string, route : string, data : any, password?: string) {
		const url : string = await this.url(route)
		const httpOptions = {
			headers: this.sessionHeaders()
		}
		const response = await this.http[method](url, data, httpOptions).toPromise()
    return response
	}

	async post (route: string, data : any, headers? : {}) {
		const url : string = await this.url(route)
		let httpOptions = {
			headers: this.sessionHeaders()
		}

		Object.keys(headers).forEach(key => {
			httpOptions.headers.set(key, headers[key])
		})

		const response = await this.http.post(url, data, httpOptions).toPromise()
    return response
	}

	async storeIpfs (data : any, encode? : boolean) {
		const ipfs = await this.ipfs()
		if (typeof data != 'string') {
			data = (data instanceof ArrayBuffer) ? data : JSON.stringify(data)
		}
		const result = await ipfs.files.add(Buffer.from(data))
		console.log(result)
		const hash = result[0].hash
		if (!encode) return hash
		return this.storage.base58ToHex(hash)
	}

	async ipfsRequest (hash) {
		const ipfs = await this.ipfs()
		const file = await ipfs.files.cat(hash)
		return file
	}

	async ipfsLink (hash) {
    return this.ipfsUrl + hash
	}

}
