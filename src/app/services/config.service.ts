import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { isInDevMode } from '../app.constants'

import config from '../../environments/environment'

@Injectable()
export class ConfigService {

  configUrl : string = config.urlEnv + `/services/${isInDevMode() ? 'dev' : 'master'}.json`
  config : any

  adresses : any
  adressesUrl : any = config.urlEnv + `/contracts/${isInDevMode() ? 'dev' : 'master'}.json`

  abiUrl : string = config.urlEnv + '/contracts/abi'

  constructor (private http: HttpClient) {}

  async getLinks () {
    if (this.config) return this.config
    console.log('isDevMode', isInDevMode())
    const data = await this.http.get(this.configUrl).toPromise()
    this.config = data
    return data
  }

  async getAdresses () {
    if (this.adresses) return this.adresses

    const data = await this.http.get(this.adressesUrl).toPromise()
    this.adresses = data
    return data
  }

  async getAbi (name : string) {
    const url = `${this.abiUrl}/${name}.json`
    return await this.http.get(url).toPromise()
  }
}
