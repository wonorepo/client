import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';
import { AccountService } from './services/account.service';
import { StorageService } from './services/storage.service';
import { ApiService } from './services/api.service';
import { ContractService } from './services/contract.service';
import { ConfigService } from './services/config.service';
import { SharedService } from './services/shared.service';
import { DialogService } from './components/dialog/dialog.service';
import { ErrorWindow } from './components/error/error.component';

import * as io from 'socket.io-client';
import * as bowser from 'bowser';
import supportedBrowsers from '../environments/browsers';
import {LocalStorageConstants} from './config/localStorage.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.template.html',
  styleUrls: ['./app.styles.scss']
})
export default class AppComponent implements OnInit {
  public static readonly CategoryChangeName = 'categoryChange';
  public static readonly CloseFilters = 'closeFilters';
  public static readonly ChatsUpdated = 'chatsUpdate'; // Создать сервисы с ReplaySubject'ами, расшарить данные между компонентами

  public balance: string;
  public search = false;
  public balanceFailed = false;
  public socket: any;
  public notifications: any[] = [];
  public unreadedCount = 0;
  public socketProcessed = false;

  public filtered = false;
  public filterOpened = false;
  @ViewChild('searchInput') public searchInput: ElementRef;

  public filter: any = {
    category : 1
  };

  public close = () => this.filterOpened = false;

  constructor(
                private router: Router,
                private configService: ConfigService,
                private account: AccountService,
                private contract: ContractService,
                private dialog: DialogService,
                private api: ApiService,
                private storage: StorageService,
                private _sharedService: SharedService,
                iconRegistry: MatIconRegistry,
                sanitizer: DomSanitizer) {
    this.updateNotifications();
    _sharedService.register(AppComponent.CategoryChangeName).subscribe(
        category => {
          this.filter.category = category;
          this.filter.category == 4 ? this.filterOpened = true : this.filterOpened = false;
        });
    this._sharedService.register(AppComponent.CloseFilters).subscribe(() => {
      this.search = false;
    });
    this._sharedService.register(AppComponent.ChatsUpdated).subscribe(() => this.updateNotifications());
    iconRegistry
      .addSvgIcon('menu', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/burger.svg'))
      .addSvgIcon('search', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/search.svg'))
      .addSvgIcon('messages', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/messages.svg'))
      .addSvgIcon('notifications', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/notifications.svg'))
      .addSvgIcon('assets', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/assets.svg'))
      .addSvgIcon('cogs', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/cogs.svg'))
      .addSvgIcon('logout', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/logout.svg'))
      .addSvgIcon('photo', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/photo.svg'))
      .addSvgIcon('confirm', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/confirm.svg'))
      .addSvgIcon('civic', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/civic.svg'))
      .addSvgIcon('star', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/star.svg'))
      .addSvgIcon('car', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/car.svg'))
      .addSvgIcon('house', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/house.svg'))
      .addSvgIcon('job', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/job.svg'))
      .addSvgIcon('update', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/update.svg'))
      .addSvgIcon('deal', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/deal.svg'))
      .addSvgIcon('chat', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/chat.svg'))
      .addSvgIcon('coins', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/coins.svg'))
      .addSvgIcon('arrow', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.svg'))
      .addSvgIcon('back', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/back.svg'))
      .addSvgIcon('clip', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/clip.svg'))
      .addSvgIcon('lock', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/lock.svg'))
      .addSvgIcon('close', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/close.svg'))
      .addSvgIcon('copy', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/copy.svg'))
      .addSvgIcon('pack', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/pack.svg'))

      .addSvgIcon('wallet-art', sanitizer.bypassSecurityTrustResourceUrl('assets/arts/wallet.svg'))
      .addSvgIcon('newwallet-art', sanitizer.bypassSecurityTrustResourceUrl('assets/arts/newwallet.svg'))
      .addSvgIcon('recovery-art', sanitizer.bypassSecurityTrustResourceUrl('assets/arts/recoveraccess.svg'))
      .addSvgIcon('meet_1', sanitizer.bypassSecurityTrustResourceUrl('assets/arts/meet_1.svg'))
      .addSvgIcon('meet_2', sanitizer.bypassSecurityTrustResourceUrl('assets/arts/meet_2.svg'))
      .addSvgIcon('meet_3', sanitizer.bypassSecurityTrustResourceUrl('assets/arts/meet_3.svg'))
      .addSvgIcon('unsuc', sanitizer.bypassSecurityTrustResourceUrl('assets/arts/unsuc.svg'))

      .addSvgIcon('placeholder', sanitizer.bypassSecurityTrustResourceUrl('assets/img/placeholder.svg'))
      .addSvgIcon('user-ph', sanitizer.bypassSecurityTrustResourceUrl('assets/img/user.svg'));
  }

  public ngOnInit(): void {
    this.filtered = !!localStorage.getItem(LocalStorageConstants.filterStorage);
    this.loadBalance();
    const browser = bowser.getParser(window.navigator.userAgent);
    const isValidBrowser = browser.satisfies(supportedBrowsers);
    if (!isValidBrowser) {
      this.dialog.open(ErrorWindow, { message: 'Please update your browser!' });
    }
  }

  public updateNotifications(): void {
    this.api.request(`profiles/${this.account.cachedUserData.address}/chats`).then((chats: any) => {
      this.unreadedCount = 0;
      chats.chats.forEach((chat) => this.unreadedCount += chat.unreadedMsg);
      if (this.unreadedCount > 0) {
        this.notifications.push({});
      }
    });
  }

  get user() {
    return this.account.cachedUserData;
  }

  get authorized() {
    const value = this.account.authorized;
    if (value && this.balanceFailed) { this.loadBalance(); }
    if (value && !this.socket) { this.initSocket(); }
    return value;
  }

  async initSocket() {
    if (this.socketProcessed) { return; }
    this.socketProcessed = true;

    const links = await this.configService.getLinks();
    const socket = this.socket = io(links.chat + '?eth_address=' +  this.user.address);
    socket.on('connect', () => {
      socket.emit('init', data => {
        console.log(data);
        this.socketProcessed = false;
      });
    });
    socket.on('notification', data => {
      this.updateNotifications();
      // this.notifications.push(data); - для прочих уведомлений (когда будет реализовано)
      console.log(data);
    });
    socket.on('error', error => {
      console.error(error);
      this.socketProcessed = false;
    });
  }

  get includes() {
    const url = this.router.url.split('/')[1];
    const exceptions = ['auth', 'chat'];
    const result = exceptions.includes(url);
    if (result) { this.notifications = []; }
    return !result;
  }

  async loadBalance() {
    this.balanceFailed = false;
    try {
      console.log('Updating balance...');
      const balance = await this.contract.balance(this.storage.getItem('address'));
      this.balance = balance.substr(0, 7);
      console.log('Balance updated.');
    } catch (error) {
      console.log(error);
      this.balanceFailed = true;
    }
  }

  public setFilterCategory(category: number): void {
    this.filter.category = category;
    this._setStorageFilters();
  }

  public searchQuery(query): void {
    console.log('searchQuery')
    console.log(query)
    this._setStorageFilters();
    const filter = JSON.parse(JSON.stringify(this.filter)); // deep clone of object (need to implement lodash)
    Object.keys(filter).forEach((key) => {
      if (!filter[key]) {
        delete filter[key];
      }
    });
    const filterValue = JSON.stringify(filter);
    this.router.navigate(['assets'],  { queryParams: { search: query, filter: filterValue } });
  }

  public clearFilters(): void {
    const filterStorage = JSON.parse(localStorage.getItem(LocalStorageConstants.filterStorage));
    delete filterStorage[this.filter.category];
    localStorage.setItem(LocalStorageConstants.filterStorage, JSON.stringify(filterStorage));
    this.filter = {category: this.filter.category};
    this.searchQuery(this.searchInput.nativeElement.value);
  }

  logout(sidenav) {
    this.account.logout();
    this.router.navigate(['']);
    sidenav.close();
  }

  state(state) {
    return this.router.url.includes(state);
  }

  private _setStorageFilters(): void {
    const storageFilter = JSON.parse(localStorage.getItem(LocalStorageConstants.filterStorage) || '{}');
    const hasCurrentFilter = !!storageFilter && !!storageFilter[this.filter.category];
    this.filter = hasCurrentFilter ? storageFilter[this.filter.category] : this.filter;
    this.filtered = hasCurrentFilter;
  }
}
