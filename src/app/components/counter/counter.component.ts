import { Component, Input, forwardRef, HostBinding } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'wono-counter',
  template: `
    <button orange circle (click)="changeValue(step * -1)">-</button>
    <span>{{value}}</span>
    <button orange circle (click)="changeValue(step)">+</button>
  `,
  styleUrls: ['./counter.styles.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => WonoCounter),
    multi: true
  }]
})
export default class WonoCounter implements ControlValueAccessor {
  public get max(): number {
    return this._max;
  }

  @Input() public set max(value: number) {
    if (!!value) {
      this._max = value;
    }
  }
  public get min(): number {
    return this._min;
  }

  @Input() public set min(value: number) {
    if (!!value) {
      this._min = value;
    }
  }

  public value = 1;
  @Input() public negative = false;
  @Input() public step = 1;
  private _min = 1;
  private _max = 100;

  @Input() disabled = false;
  @HostBinding('style.opacity')
  public get opacity () {
    return this.disabled ? 0.25 : 1;
  }

  public onChange = (value: number) => {};

  public writeValue(value: any): void {
    if (!!value) {
      this.value = value;
    }
  }

  public changeValue(value: number): void {
    if (this.disabled) {
      return;
    }
    value < 0 ? this.subtract() : this.add();
    this.onChange(this.value);
  }

  public add(): void {
    const tempValue = this.value + this.step;
    this.value = tempValue >= this._max ? this._max : tempValue;
  }

  public subtract(): void {
    const tempValue = this.value - this.step;
    this.value = tempValue <= this._min ? this._min : tempValue;
  }

  public registerOnChange(fn: (rating: number) => void): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void): void {}

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
