import { Component, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'

@Component({
  selector: 'dialog',
  templateUrl: './dialog.template.html',
  styleUrls: ['./dialog.styles.css']
})
export default class Dialog {

  constructor(
    public dialogRef: MatDialogRef<Dialog>,
    @Inject(MAT_DIALOG_DATA) public data : any) {}

}
