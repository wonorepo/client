import { Injectable } from '@angular/core'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'

import Dialog from './dialog.component'

@Injectable()
export class DialogService {

  constructor (public dialog: MatDialog) {}

  open (component : any, data? : any) {
    const dialogRef = this.dialog.open(component, { data })
    return new Promise((resolve, reject) => {
      dialogRef.afterClosed().subscribe(result => {
        resolve(result)
      })
    })
  }

  multiple (components : any[]) {
    return this.open(Dialog, { pages: components })
  }
}
