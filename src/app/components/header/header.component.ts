import { Component, Input } from '@angular/core';

@Component({
  selector: 'page-header',
  template: `
    <div></div>
    <h2>{{title}}</h2>
    <ng-content></ng-content>
  `,
  styleUrls: ['./header.component.scss']
})
export default class PageHeaderComponent {
  @Input() title = '';
}
