import { Component, Input } from '@angular/core'
import { Router } from '@angular/router'
import { MatSnackBar } from '@angular/material'
import { ActivatedRoute } from '@angular/router'
import { ConfirmWindow } from '../../components/confirm/confirm.component'
import { DialogService } from '../../components/dialog/dialog.service'
import { AssetService } from '../../services/asset.service'

@Component({
  selector: 'asset-item',
  templateUrl: './asset.template.html',
  styleUrls: ['./asset.styles.css']
})
export default class AssetItem {

  protected avatar : string = 'assets/img/user.svg'
  loading : boolean = true
  _draft : boolean
  extras : any[] = []
  id
  @Input() asset : any
  @Input() ismainpage : boolean
  @Input() own : boolean
  @Input() authorized : boolean
  @Input() user : any

  constructor ( private router: Router,
                private assetService: AssetService,
                private dialog: DialogService,
                public snackBar: MatSnackBar,
                private route: ActivatedRoute)
  {}


  ngOnInit() {
    this.id = this.asset.id
    this._draft = this.asset.draft
  	this.extras = Object.keys(this.asset.extras)
  }
  async deleteAsset () {
    const password : any = await this.dialog.open(ConfirmWindow)
    if (!password) return

    this.loading = true
    const deaft = await this.assetService.deleteAsset(this.id,this._draft)
    this.snackBar.open('Asset'+(this._draft ? '-Draft ':' ')+ 'successfuly deleted!', '', {
      duration: 5000,
    })
    this.loading = false
    this.router.navigate(['/assets?own=true'])
  }
}
