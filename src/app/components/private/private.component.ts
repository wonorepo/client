import { Component, Inject } from '@angular/core'
import { StorageService } from '../../services/storage.service'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'

@Component({
  selector: 'private-settings',
  templateUrl: './private.template.html',
  styleUrls: ['./private.styles.css']
})
export class PrivateSettingsWindow {

  fields : string[] = []
  user : any
  encrypted : string[] = []
  decrypted : string[] = []
  invalidMessage : string = ''

  constructor(public dialogRef: MatDialogRef<PrivateSettingsWindow>,
              private storage : StorageService,
              @Inject(MAT_DIALOG_DATA) public data : any) {
    this.user = data.user
    this.fields = Object.keys(data.user).filter(key => {
      if (!data.user[key].hasOwnProperty('private')) return false
      data.user[key].private ? this.encrypted.push(key) : this.decrypted.push(key)
      return key
    })
  }

  process (password : string) {
    this.invalidMessage = ''
    try {
      this.storage.verifyPassword(password)
      const privateKey = this.storage.privateKey(password)
      this.decrypt(privateKey)
      this.encrypt(privateKey)
      this.dialogRef.close(this.user)
    } catch (error) {
      this.invalidMessage = error.message
    }
  }

  encrypt (privateKey : string) {
    this.fields.filter(field => this.user[field].private).forEach(field => {
      if (this.encrypted.includes(field)) return
      this.user[field].value = this.storage.encrypt(this.user[field].value, privateKey)
    })
  }

  decrypt (privateKey : string) {
    this.fields.filter(field => !this.user[field].private).forEach(field => {
      if (this.decrypted.includes(field)) return
      this.user[field].value = this.storage.decrypt(this.user[field].value, privateKey)
    })
  }

}
