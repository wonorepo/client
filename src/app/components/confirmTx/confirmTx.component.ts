import { Component } from '@angular/core'
import { MatDialogRef } from '@angular/material'
import { ContractService } from '../../services/contract.service'
import { StorageService } from '../../services/storage.service'

@Component({
  selector: 'confirm-tx',
  templateUrl: './confirmTx.template.html',
  styleUrls: ['./confirmTx.styles.css']
})
export class ConfirmTxWindow {

  private invalid : string = ''
  private gasAmount : number = 20
  private balance : string = '0'
  private _balance : string

  constructor(
    public dialogRef: MatDialogRef<ConfirmTxWindow>,
    private contract: ContractService,
    private storage: StorageService) {}

  async ngOnInit () {
    this._balance = await this.contract.balance(this.storage.getItem('address'))
    this.balance = this._balance.substr(0, 9)
  }

  close (password) {
    this.dialogRef.close(password)
  }

  async confirm (password, gasAmount) {
    this.invalid = ''
    if (this.balance == '0') {
      this.invalid = 'Not enough money'
      return
    }
    const web3 = await this.contract.getWeb3()
    const balanceWei = web3.utils.toWei(this._balance, 'ether')
    const gasWei = web3.utils.toWei(gasAmount.toString(), 'gwei')
    if (balanceWei < gasWei) {
      this.invalid = 'Not enough money'
    }
    const { address, privateKey} = this.storage.walletData(password)
    if (privateKey) this.close({
      address: address,
      privateKey: privateKey,
      gasAmount: this.gasAmount,
      password: password
    })
    else this.invalid = 'Password is incorrect!'
  }

}
