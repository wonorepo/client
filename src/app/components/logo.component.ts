import { Component, Input } from '@angular/core'

import { DomSanitizer } from '@angular/platform-browser'
import { MatIconRegistry } from '@angular/material'

@Component({
  selector: 'wono-logo',
  template: `
    <mat-icon svgIcon="logo" [class.full]="full"></mat-icon>
    <mat-icon svgIcon="wono" *ngIf="full"></mat-icon>
  `,
  styles: [`
    :host {
      display: flex;
      height: 5em;
      align-items: center;
    }
    mat-icon {
      align-self: stretch;
      height: auto;
      flex: 1 1 auto;
    }
    .full {
      margin: 1.2rem 0;
    }
  `]
})
export default class WonoLogo {

  @Input() full : boolean = true;

  constructor (iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry
      .addSvgIcon('logo', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/logo.svg'))
      .addSvgIcon('wono', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/wono.svg'))
  }

}
