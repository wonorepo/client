import { Component, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'

@Component({
  selector: 'error-window',
  templateUrl: './error.template.html',
  styleUrls: ['./error.styles.css']
})
export class ErrorWindow {

  constructor(public dialogRef: MatDialogRef<ErrorWindow>, @Inject(MAT_DIALOG_DATA) public data : any) {}

  close () {
    this.dialogRef.close()
  }

}
