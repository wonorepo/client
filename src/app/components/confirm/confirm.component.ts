import { Component } from '@angular/core'
import { MatDialogRef } from '@angular/material'
import { StorageService } from '../../services/storage.service'

@Component({
  selector: 'confirm-window',
  templateUrl: './confirm.template.html',
  styleUrls: ['./confirm.styles.css']
})
export class ConfirmWindow {

  private invalid : boolean = false

  constructor(public dialogRef: MatDialogRef<ConfirmWindow>, private storage: StorageService) {}

  close (password) {
    this.dialogRef.close(password)
  }

  verify (password) {
    this.invalid = false
    const result = this.storage.verifyPassword(password)
    if (result) this.close(password)
    else this.invalid = true
  }

}
