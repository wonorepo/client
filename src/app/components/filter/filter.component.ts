import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, FormArray } from '@angular/forms';

import { Store } from '../../services/store.service';

import appartmentFields from '../../config/appartment.fields'
import transportFields from '../../config/transport.fields'
import resumeFields from '../../config/resume.fields'
import {LocalStorageConstants} from '../../config/localStorage.constants'


@Component({
  selector: 'app-filter-window',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterWindowComponent implements OnInit {

  public priceConfig: any = {
    behaviour: 'drag',
    connect: true,
    start: [300, 500],
    range: {
      min: 0,
      max: 1000
    },
    margin: 50,
    tooltips: [true, true],
    step: 1
  };
  public showMoreFilters: false;
  public fields: any;
  public forms: any;

  @Input() public filter: any;
  @Output() public filtered = new EventEmitter<void>();
  @Output() public close = new EventEmitter<void>();

  constructor(private store: Store) {}

  public ngOnInit(): void {
    this.forms = {
      1: this.toFormGroup(appartmentFields, 1),
      2: this.toFormGroup(transportFields, 2),
      4: this.toFormGroup(resumeFields, 4)
    };
    this.fields = {
      1: appartmentFields,
      2: transportFields,
      4: resumeFields
    };
  }

  public applyFilters(form): void {
    const filterStorage = JSON.parse(localStorage.getItem(LocalStorageConstants.filterStorage)) || {};
    filterStorage[this.filter.category] = form.value;
    localStorage.setItem(LocalStorageConstants.filterStorage, JSON.stringify(filterStorage));
    this.closeFilters();
    this.filtered.emit();
  }

  public closeFilters(): void {
    if(this.filter.category != 4) {
      this.close.emit();
    }
  }

  countSkillsForm(max = 5){
    const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);
      return totalSelected <= max ? null : { required: true };
    }
    return validator
  }

  public toFormGroup(fields: any[], filter: number): FormGroup {

    const group: any = {
      category: new FormControl(filter)
    };
    if (filter != 4) group.price = new FormControl('')

    if (filter == 4) group.location = new FormControl('')

    fields.forEach(field => {
      if(field.type === 'groupOfCheckbox'){
        const controls = field.values.map(c => new FormControl(false));
        group[field.name] = new FormArray(controls, this.countSkillsForm(5))
      } else {
        group[field.name] = new FormControl(field.value || '')
      }
    })


    const formGroup = new FormGroup(group);
    console.log('NEED TO REWRITE LOAD FILTER FROM LOCALSTORAGE')
     /* WARNING NEED TO REWRITE
    const filterStorage = JSON.parse(localStorage.getItem(LocalStorageConstants.filterStorage)) || {};
    const filterByCategory = filterStorage[filter];
    if (!!filterByCategory) {
      console.log(JSON.stringify(filterByCategory, null, 4))
      formGroup.setValue(filterByCategory);
    }
    */
    return formGroup;
  }

  public setValue(data): void {
    this.store.dispatch(data);
  }

  public updateValue(data): void {
    this.store.dispatch(data);
  }

  public deleteValue(key): void {
    this.store.dispatch('DELETE');
  }
}
