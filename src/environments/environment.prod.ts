const environment = {
  production: true,
  urlEnv: 'https://env.wono.io'
}

export default environment
