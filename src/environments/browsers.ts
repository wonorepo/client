const supportedBrowsers = {
  windows: {
    "internet explorer": ">10"
  },
  macos: {
    "safari": ">10.1"
  },
  mobile: {
    "safari": ">9",
    "android browser": ">3.10"
  },
  chrome: ">20.1.1432",
  firefox: ">31",
  opera: ">22"
}

export default supportedBrowsers
