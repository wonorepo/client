#!/bin/bash

# this rebuild script is called by pipelines when testing and deployment occures


delimeter_script="============================================================="
delimeter_action="_____________________________________________________________"
delimeter_command="-------------------------------------------------------------"


do_cmd() {
    echo -e "COMMAND TO EXECUTE: $@\n"
    $@ 2>&1
    code=$?
    if [ $code != '0' ]
    then
        exit $code
    fi
}


app="platform client"
env="dev";
commit="HEAD"
[[ "\\$(git branch)" =~ "detached" ]] && branch="dev" || branch=$(git symbolic-ref --short $commit)


while getopts ":e:b:c:u:k:t:" opt; do
    case $opt in
        e) env="$OPTARG";;
        b) branch="$OPTARG";;
        c) commit="$OPTARG";;
        u) user="$OPTARG";;
        k) key="$OPTARG";;
        t) token="$OPTARG";;
        \?) echo "Invalid option -$OPTARG" >&2;;
    esac
done


echo -e "\n$delimeter_script"
echo -e "building $app has been started"
echo -e "$delimeter_script\n"


echo -e "$delimeter_action"
echo -e "setting NODE_ENV"
echo -e "$delimeter_action\n"


do_cmd export "NODE_ENV=$env"


echo -e "\n$delimeter_action"
echo -e "updating $app repo"
echo -e "$delimeter_action\n"


do_cmd git checkout $branch
echo -e "\n$delimeter_command\n"
do_cmd git pull
echo -e "\n$delimeter_command\n"
do_cmd git reset $commit


echo -e "\n$delimeter_action"
echo -e "\ninstalling $app packages\n"
echo -e "$delimeter_action\n"


do_cmd export "NODE_ENV=dev" # tokensale works fine on dev mode only
echo -e "\n$delimeter_command\n"
do_cmd yarn install
echo -e "\n$delimeter_command\n"
do_cmd export "NODE_ENV=$env" # re-export then


echo -e "\n$delimeter_action"
echo -e "\nbuilding $app\n"
echo -e "$delimeter_action\n"


do_cmd yarn build:$env


if [ -n "$token" ]
then
    echo -e "\n$delimeter_action"
    echo -e "\nresetting cloudflare cache\n"
    echo -e "$delimeter_action\n"


    do_cmd "curl -X POST https://api.cloudflare.com/client/v4/zones/$token/purge_cache -HX-Auth-Email:$user -HX-Auth-Key:$key -HContent-Type:application/json --data {\"purge_everything\":true}"
    echo ""
fi


echo -e "\n$delimeter_script"
echo -e "building $app has been finished successfully"
echo -e "$delimeter_script\n"

